#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
//
#include "Components/StaticMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
//
#include "ChessPawn.generated.h"

UCLASS()
class CHESS_FOR_PINGLE_API AChessPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AChessPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


public:
	// ...
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UStaticMeshComponent* mesh;

	// ...
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	USpringArmComponent* springArm;

	// ...
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCameraComponent* camera;
};
