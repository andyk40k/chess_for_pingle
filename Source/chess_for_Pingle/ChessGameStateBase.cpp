#include "ChessGameStateBase.h"
#include "Kismet//GameplayStatics.h"

AChessGameStateBase::AChessGameStateBase() {

}

void AChessGameStateBase::BeginPlay()
{
	Super::BeginPlay();
	//
	auto world = GetWorld();

	if (!world) {
		return;
	}

	TArray<AActor*> foundResult;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AChessboard::StaticClass(), foundResult);

	if (foundResult.Num() == 1) {
		chessboard = Cast<AChessboard>(foundResult[0]);
	}
}
