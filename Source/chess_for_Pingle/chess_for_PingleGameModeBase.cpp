#include "chess_for_PingleGameModeBase.h"
//
#include "ChessGameStateBase.h"
#include "ChessPlayerController.h"
#include "ChessPawn.h"
#include "Engine/World.h"

Achess_for_PingleGameModeBase::Achess_for_PingleGameModeBase(){
	GameStateClass = AChessGameStateBase::StaticClass();
	PlayerControllerClass = AChessPlayerController::StaticClass();
	//DefaultPawnClass = AChessPawn::StaticClass();
}

void Achess_for_PingleGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	//
}

void Achess_for_PingleGameModeBase::StartPlay()
{
	Super::StartPlay();

	
}

void Achess_for_PingleGameModeBase::CreateNewChessGame()
{
	chessGame = NewObject<UChessGameInstance>(this, "chessGame");
}
