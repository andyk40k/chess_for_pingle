#pragma once

#include "CoreMinimal.h"
//
#include "GameFramework/GameStateBase.h"
#include "ChessGame/AChessboard.h"
//
#include "ChessGameStateBase.generated.h"

UCLASS()
class CHESS_FOR_PINGLE_API AChessGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	AChessGameStateBase();

	virtual void BeginPlay() override;

	inline AChessboard *GetChessboard() { return chessboard; }

private:
	UPROPERTY()
	AChessboard *chessboard;
};
