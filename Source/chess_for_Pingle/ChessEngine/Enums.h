#pragma once

#include "CoreMinimal.h"

#include "Enums.generated.h"

#pragma optimize ("", off)
/**
 * 

class CHESS_FOR_PINGLE_API Enums
{
public:
	Enums();
	~Enums();
};
 */

UENUM(BlueprintType)
enum class EChessFigureType : uint8 {
	None    = 0		UMETA(DisplayName = "???"),
	Pawn    = 10	UMETA(DisplayName = "Пешка (Pawn)"),
	Knight  = 20	UMETA(DisplayName = "Конь (Knight)"),
	Bishop  = 30	UMETA(DisplayName = "Слон (Bishop)"),
	Rook    = 40	UMETA(DisplayName = "Ладья (Rook)"),
	Queen   = 50	UMETA(DisplayName = "Ферзь (Queen)"),
	King    = 60	UMETA(DisplayName = "Король (King)"),
};

UENUM(BlueprintType)
enum class EChessTeam : uint8 {
	None   = 0		UMETA(DisplayName = "None"),
	Team1  = 10		UMETA(DisplayName = "Team1"),
	Team2  = 100	UMETA(DisplayName = "Team2"),
};

UENUM(BlueprintType)
enum class EChessGameState : uint8 {
	None = 0	     	UMETA(DisplayName = "None"),
	Normal = 10	    	UMETA(DisplayName = "Normal"),
	Check = 20		    UMETA(DisplayName = "Check"),
	Checkmate = 100		UMETA(DisplayName = "Checkmate"),
};

UENUM(BlueprintType)
enum class EChessCellState : uint8 {
	None      = 0	UMETA(DisplayName = "None"),
	Select    = 1	UMETA(DisplayName = "Select"),
	Attack    = 2	UMETA(DisplayName = "Attack"),
};

USTRUCT()
struct CHESS_FOR_PINGLE_API FCellAction {
	GENERATED_USTRUCT_BODY()

	FCellAction() = default;

	FCellAction(const uint8 &_uid, const EChessCellState &_action)
		:
		uid(_uid),
		action(_action)
	{}

	uint8 uid{ 0 };
	EChessCellState action{ EChessCellState::None };

	bool operator==(const FCellAction&_other) {
		return (uid == _other.uid) && (action == _other.action);
	}
};

struct MoveRule {
	int rule;
	int group;
};

struct CoordsXY {
	uint8 x;
	uint8 y;

	bool operator==(const CoordsXY &_other) const {
		return ((x == _other.x) && (y == _other.y));
	}
};

// Превращение в 2х мерные координаты
inline static CoordsXY ToXY(const uint8 &_position) {
	return CoordsXY{
		static_cast<uint8>(FMath::FloorToInt(_position % 8)),
		static_cast<uint8>(FMath::CeilToInt(_position / 8))
	};
}
// Структура для извлечеиня данных из масива поля
struct GamefieldCellInfo {
	EChessFigureType type;
	EChessTeam team;
	uint8 position;

	uint8 ToUint8() {
		return (static_cast<uint8>(type) + static_cast<uint8>(team));
	}
};
// ...
static GamefieldCellInfo GetCellInfo(const TArray<uint8> &_gamefield, const uint8 &_position) {
	auto obj = _gamefield[_position];
	//
	if (obj == 0) {
		return { EChessFigureType::None, EChessTeam::Team1, _position };
	}
	GamefieldCellInfo returnData;

	returnData.type = static_cast<EChessFigureType>(obj < static_cast<uint8>(EChessTeam::Team2) ? (obj - static_cast<uint8>(EChessTeam::Team1)) : (obj - static_cast<uint8>(EChessTeam::Team2)));
	returnData.team = (obj < static_cast<uint8>(EChessTeam::Team2) ? EChessTeam::Team1 : EChessTeam::Team2);
	returnData.position = _position;

	return returnData;
};
#pragma optimize ("", on)