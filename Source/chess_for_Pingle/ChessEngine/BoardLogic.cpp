#include "BoardLogic.h"

#pragma optimize ("", off)
void BoardLogic::GetPossibleMoves(TArray<MoveRule> &_outputData, const uint8 &_boardPosition, const EChessFigureType &_figureType, const EChessTeam &_team)
{
	switch (_figureType)
	{
	case EChessFigureType::Pawn:
		Generate(RulePawn(), EChessFigureType::Pawn, _outputData, _boardPosition, _team);
		break;
	case EChessFigureType::Knight:
		Generate(RuleKnight(), EChessFigureType::Knight, _outputData, _boardPosition, _team);
		break;
	case EChessFigureType::Bishop:
		Generate(RuleBishop(), EChessFigureType::Bishop, _outputData, _boardPosition, _team);
		break;
	case EChessFigureType::Rook:
		Generate(RuleRook(), EChessFigureType::Rook, _outputData, _boardPosition, _team);
		break;
	case EChessFigureType::Queen:
		Generate(RuleQueen(), EChessFigureType::Queen, _outputData, _boardPosition, _team);
		break;
	case EChessFigureType::King:
		Generate(RuleKing(), EChessFigureType::King, _outputData, _boardPosition, _team);
		break;
	default:
		break;
	}
}


void BoardLogic::Generate(const TArray<int32> &_rules, const EChessFigureType &_figureType, TArray<MoveRule> &_outputData, const int32 &_currentFigurePos, const EChessTeam &_team)
{
	auto figurePos2d = ToXY(_currentFigurePos);
	int count = 1;
	int group = 1;
	// Направление хода
	int dirX = 0;
	int dirY = 0;
	// Группировка направлений чтобы дальше можно было найти преграду по пути
	auto calcGroup = [&]() {
		switch (_figureType)
		{
		case EChessFigureType::Pawn:
			if (count % 2 == 0) {
				++group;
			}
			break;
		case EChessFigureType::Knight:
			++group;
			break;
		case EChessFigureType::Bishop:
		case EChessFigureType::Rook:
		case EChessFigureType::Queen:
			if (count >= 7) {
				count = 1;
				++group;
				return;
			}
			break;
		case EChessFigureType::King:
			++group;
			break;
		default:
			break;
		}
		++count;
	};

	auto diagonaleChecker = [&](const CoordsXY &_obj, int _count = 7) -> bool {
		bool bResult{ false };

		for (int i = 1; i < (_count + 1); ++i) {
			if ((figurePos2d.x + (dirX * i) == _obj.x) && (figurePos2d.y + (dirY * i) == _obj.y)) {
				bResult = true;
				break;
			}
		}
		return bResult;
	};
	auto horisontalChecker = [&](const CoordsXY &_obj, int _count = 7) -> bool {
		bool bResult{ false };

		for (int i = 1; i < (_count + 1); ++i) {
			auto tmp = dirX * i;

			if ((figurePos2d.x + tmp == _obj.x) && (figurePos2d.y == _obj.y)) {
				// Это хотфикс от ложного срабатывания когда фигура - королева.
				if ((group == 3 || group == 7) && ((tmp == 7) || (tmp == -7))) {
					continue;
				}
				bResult = true;
				break;
			}
		}
		return bResult;
	};
	auto vertivalChecker = [&](const CoordsXY &_obj, int _count = 7) -> bool {
		bool bResult{ false };

		for (int i = 1; i < (_count + 1); ++i) {
			if ((figurePos2d.y + (dirY * i) == _obj.y) && (figurePos2d.x == _obj.x)) {
				bResult = true;
				break;
			}
		}
		return bResult;
	};
	auto knightChecker = [&](const CoordsXY &_obj) -> bool {
		bool bResult{ false };

		struct s {
			int x;
			int y;
		};
		static TArray<s> coords{
			{ 1,  2}, { 2,  1}, // 2 часа
			{ 2, -1}, { 1, -2}, // 4 часа
			{-1, -2}, {-2, -1}, // 8 часов
			{-2,  1}, {-1,  2}, // 11 часов
		};

		for (int i = 0; i < coords.Num(); ++i) {
			auto tmpObj = CoordsXY{
				static_cast<uint8>(figurePos2d.x + coords[i].x),
				static_cast<uint8>(figurePos2d.y + coords[i].y)
			};
			if (tmpObj == _obj) {
				bResult = true;
				break;
			}
		}
		return bResult;
	};

	auto pawnChecker = [&](const CoordsXY &_obj) -> bool {
		// 1 или 2 хода нужно сделать пешкой
		int pawnStepCount = (
			((figurePos2d.y == 1) && (_team == EChessTeam::Team1)) ||
			((figurePos2d.y == 6) && (_team == EChessTeam::Team2))
			) ? 2 : 1;
		bool bResult{ vertivalChecker(_obj, pawnStepCount) };
		bool extraCheckLeft = (((figurePos2d.x - 1) == _obj.x) && ((figurePos2d.y + (_team == EChessTeam::Team1 ? 1 : -1) == _obj.y)));
		bool extraCheckRight = (((figurePos2d.x + 1) == _obj.x) && ((figurePos2d.y + (_team == EChessTeam::Team1 ? 1 : -1) == _obj.y)));
		return (bResult || extraCheckLeft || extraCheckRight);
	};

	for (const auto &ruleI : _rules) {
		auto result = 0;
		// Особые правила для пешки ... у пешки есть направление.
		if (_figureType == EChessFigureType::Pawn) {
			result = _currentFigurePos + (ruleI * (_team == EChessTeam::Team1 ? 1 : -1));
		}
		else {
			result = _currentFigurePos + ruleI;
		}

		if (result >= 0 && result < 64) {
			auto rulePos2d = ToXY(result);
			bool bIsValidStep{ false };

			dirX = ((rulePos2d.x >= figurePos2d.x) == 1 ? 1 : -1);
			dirY = ((rulePos2d.y >= figurePos2d.y) == 1 ? 1 : -1);

			switch (_figureType)
			{
			case EChessFigureType::Bishop:
				bIsValidStep = diagonaleChecker(rulePos2d);
				break;
			case EChessFigureType::Rook:
				bIsValidStep = (vertivalChecker(rulePos2d) || horisontalChecker(rulePos2d));
				break;
			case EChessFigureType::Queen:
				bIsValidStep = (vertivalChecker(rulePos2d) || horisontalChecker(rulePos2d) || diagonaleChecker(rulePos2d));
				break;
			case EChessFigureType::King:
				bIsValidStep = (vertivalChecker(rulePos2d, 1) || horisontalChecker(rulePos2d, 1) || diagonaleChecker(rulePos2d, 1));
				break;
			case EChessFigureType::Pawn:
				bIsValidStep = pawnChecker(rulePos2d);
				break;
			case EChessFigureType::Knight:
				bIsValidStep = knightChecker(rulePos2d);
				break;
			}

			if (bIsValidStep) {
				_outputData.Add({ result, group });
			}
		}

		calcGroup();
	}
}

void BoardLogic::GetPossibleMovesOnBoard(const TArray<uint8> &_gamefield, const uint8 &_cell, TArray<FCellAction> &_outputData)
{
	auto cellState = GetCellInfo(_gamefield, _cell);

	if (cellState.type != EChessFigureType::None) {
		TArray<MoveRule> steps;

		GetPossibleMoves(steps, _cell, cellState.type, cellState.team);

		int pawnAttackGroup = -1;
		// Если встретили преграду - все что за ней игнорируется
		int activeGroup = -1;
		// Проверяем все возможные ходы
		for (const auto &i : steps) {
			auto cellInfo = GetCellInfo(_gamefield, i.rule);

			// Особые правила хода для пешки
			if (cellState.type == EChessFigureType::Pawn) {
				if (pawnAttackGroup == -1) {
					pawnAttackGroup = i.group;
				}
				// Первые 2 хода - возможность бить другие фигуры по диагонали
				// Если там нет вражеских фигур - пропускаем эти клетки
				if (i.group == pawnAttackGroup) {
					if (cellInfo.type != EChessFigureType::None) {
						if (cellInfo.team != cellState.team) {
							_outputData.Add({ static_cast<uint8>(i.rule), EChessCellState::Attack });
						}
					}
					continue;
				}

				if (i.group == activeGroup) {
					continue;
				}
				// Ячейка не пуста - можем ли сюда ходить?
				if (cellInfo.type != EChessFigureType::None) {
					activeGroup = i.group;

					continue;
				}
				// Ячейка пустая - сюда можно ходить
				else if (cellInfo.type == EChessFigureType::None) {
					_outputData.Add({ static_cast<uint8>(i.rule), EChessCellState::Select });
				}
				// Игнорируем другую часть цикла
				continue;
			}

			if (i.group == activeGroup) {
				continue;
			}

			// Ячейка не пуста - можем ли мы бить?
			if (cellInfo.type != EChessFigureType::None) {
				activeGroup = i.group;

				if (cellInfo.team != cellState.team) {
					_outputData.Add({ static_cast<uint8>(i.rule), EChessCellState::Attack });
				}
			}
			// Ячейка пустая - сюда можно ходить
			else if (cellInfo.type == EChessFigureType::None) {
				activeGroup = -1;
				_outputData.Add({ static_cast<uint8>(i.rule), EChessCellState::Select });
			}
		}
	}
}

void BoardLogic::GetPossibleMovesOnBoardForKing(const TArray<uint8>& _board, const GamefieldCellInfo &_kingState, TArray<FCellAction>& _outputData)
{
	// Сохраняем оригинальные шаги
	auto kingSteps = _outputData;
	// Безопасный ли это ход?
	auto fIsSafeStep = [&](const uint8 &_kingStep) -> bool {
		bool bReturn = true;
		TArray<FCellAction> tmpData;

		for (int i = 0; i < 64; ++i) {
			auto cellState = GetCellInfo(_board, i);

			if ((cellState.team == _kingState.team) || (cellState.type == EChessFigureType::None)) {
				continue;
			}
			GetPossibleMovesOnBoard(_board, i, tmpData);

			for (int j = 0; j < tmpData.Num(); ++j) {
				if (tmpData[j].uid == _kingStep) {
					bReturn = false;
					break;
				}
			}

			if (!bReturn) {
				break;
			}
		}

		return bReturn;
	};

	// Удаляем лишние ходы для короля когда 'шаг'
	for (int i = 0; i < kingSteps.Num(); ++i) {
		bool bIsSafeStep = fIsSafeStep(kingSteps[i].uid);

		if (!bIsSafeStep) {
			kingSteps[i].action = EChessCellState::None;
		}
	}
	// Удаляем ходы которые не прошли проверки выше
	kingSteps.RemoveAll([](const FCellAction &_i) {return (_i.action == EChessCellState::None); });

	_outputData = kingSteps;
}

TArray<int32> &BoardLogic::RulePawn()
{
	static TArray<int32> rulePawn({
		7,		// может побить на 11 часов
		9,		// может побить на 1 час
		// ---
		8,		// 1 клетка вверх
		16,		// 2 клетки вверх
		});
	return rulePawn;
}

TArray<int32> &BoardLogic::RuleKnight()
{
	static TArray<int32> ruleKnight{
		 17,	// на 1 час
		 10,	// на 2 часа
		-6,		// на 4 часа
		-15,	// на 5 часа
		-17,    // на 7 часов
		-10,    // на 8 часов
		 6,     // на 10 часов
		 15,    // на 11 часов
	};
	return ruleKnight;
}

TArray<int32> &BoardLogic::RuleBishop()
{
	static TArray<int32> ruleBishop{
		// на 1 час
		9, 18, 27, 36, 45, 54, 63,
		// на 5 часов
		-7, -14, -21, -28, -35, -42, -49,
		// на 7 часов
		-9, -18, -27, -36, -45, -54, -63,
		// на 11 часов
		7, 14, 21, 28, 35, 42, 49,
	};
	return ruleBishop;
}

TArray<int32> &BoardLogic::RuleRook()
{
	static TArray<int32> ruleRook{
		// на 3 часа
		1, 2, 3, 4, 5, 6, 7,
		// на 6 часов
		-8, -16, -24, -32, -40, -48, -56,
		// на 9 часов
		-1, -2, -3, -4, -5, -6, -7,
		// на 12 часов
		8, 16, 24, 32, 40, 48, 56,
	};
	return ruleRook;
}

TArray<int32> &BoardLogic::RuleQueen()
{
	static TArray<int32> ruleQueen{
		// на 1 час
		9, 18, 27, 36, 45, 54, 63,
		// на 3 часа
		1, 2, 3, 4, 5, 6, 7,
		// на 5 часов
		-7, -14, -21, -28, -35, -42, -49,
		// на 6 часов
		-8, -16, -24, -32, -40, -48, -56,
		// на 7 часов
		-9, -18, -27, -36, -45, -54, -63,
		// на 9 часов
		-1, -2, -3, -4, -5, -6, -7,
		// на 11 часов
		7, 14, 21, 28, 35, 42, 49,
		// на 12 часов
		8, 16, 24, 32, 40, 48, 56,
	};
	return ruleQueen;
}

TArray<int32> &BoardLogic::RuleKing()
{
	static TArray<int32> ruleKing{
		 9, // на 1 час
		 1, // на 3 час
		-7, // на 5 часов
		-8, // на 6 часов
		-9, // на 7 часов
		-1, // на 9 часов
		 7, // на 11 часов
		 8, // на 12 часов
	};
	return ruleKing;
}
