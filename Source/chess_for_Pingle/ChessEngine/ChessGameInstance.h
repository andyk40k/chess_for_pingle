﻿#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
//
#include "Enums.h"
#include "IChessboard.h"
//
#include "ChessGameInstance.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnGameStateChange, EChessTeam, team, EChessGameState, oldState, EChessGameState, newState);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnPlayerMakeStep, EChessTeam, team, uint8, from, uint8, to);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnBeginGame, EChessTeam, team);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSelectCell, EChessFigureType, figure, EChessTeam, team, uint8, position);
/**
 *
 */
UCLASS()
class CHESS_FOR_PINGLE_API UChessGameInstance : public UObject
{
	GENERATED_BODY()
	
public:
	UChessGameInstance();

public:
	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "-Chess|Game")
	void StartNewGame(UPARAM(ref) TScriptInterface<IIChessboard>& _board);

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "-Chess|Game")
	void RestartGame();

	UFUNCTION()
	bool SelectFigure(const uint8 &_position);

	UFUNCTION()
	void ClearFigure(const uint8 &_position);

	UFUNCTION()
	bool ValidateMove(const uint8 &_positionFrom, const uint8 &_positionTo);

	UFUNCTION()
	void MakeMove(const uint8 &_positionFrom, const uint8 &_positionTo);
	 
	// ...
	UFUNCTION()
	FORCEINLINE EChessGameState &GetGameState() { return gameState; }

	// ...
	UFUNCTION()
	FORCEINLINE EChessTeam &GetCurrentTeam() { return currentTeamPlay; }

public:
	// Когда начали игру
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "-Chess|Game|Delegate")
	FOnBeginGame onBeginGame;

	// Когда поменялось состояние игры
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "-Chess|Game|Delegate")
	FOnGameStateChange onGameStateChange;

	// Когда игрок сделал ход
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "-Chess|Game|Delegate")
	FOnPlayerMakeStep onPlayerMakeStep;	

	// Когда игрок выбрал ячейку
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "-Chess|Game|Delegate")
	FOnSelectCell onSelectCell;
	

private:
	// Начальное игровое поле
	static TArray<uint8> GetInitGamefield();

	// Возможные шаги для выбраной фигурой
	void PrepareFigureSteps(const uint8 &_cell, bool bIsKing);

	// Просто очистить массив
	FORCEINLINE void ClearFigureSteps() {
		figureSteps.Reset();
	};

	// Текущие 
	inline TArray<FCellAction> &GetFigureSteps() {
		return figureSteps;
	};

	// Поиск короля.
	GamefieldCellInfo FindKing(const EChessTeam &_team);

	// Обновить данные в ячейке на доске
	FORCEINLINE void Gamefield_Update(const uint8 &_position, const uint8 &_data) {
		gamefield[_position] = _data;
	}

	// Дополнительные проверки после хода
	void PostMoveCheck();

	void ChangeGameState(const EChessGameState &_newState, bool bIsNeedBroadcast = true);

	// Шаг окончен, даем ход другому игроку.
	void StepEnd();

	// Наш AI :)
	void AiStep();

private:
	// Текущее состояние игры
	UPROPERTY()
	EChessGameState gameState;

	// Текущая команда
	UPROPERTY()
	EChessTeam currentTeamPlay;

	// Текущее игровое поле
	UPROPERTY()
	TArray<uint8> gamefield;

	// Ref на доску
	UPROPERTY()
	TScriptInterface<IIChessboard> board;

	// Активные возможные ходы
	UPROPERTY()
	TArray<FCellAction> figureSteps;
};
