﻿#include "ChessGameInstance.h"
//
#include "BoardLogic.h"

#pragma optimize ("", off)
UChessGameInstance::UChessGameInstance()
{
	gameState = EChessGameState::None;
	currentTeamPlay = EChessTeam::None;
}

void UChessGameInstance::StartNewGame(UPARAM(ref) TScriptInterface<IIChessboard>& _board)
{
	board = _board;
	gamefield = GetInitGamefield();
	board->InitBoard(gamefield);
	gameState = EChessGameState::Normal;
	currentTeamPlay = EChessTeam::Team1;

	onBeginGame.Broadcast(currentTeamPlay);
}

void UChessGameInstance::RestartGame()
{
	StartNewGame(board);
}

bool UChessGameInstance::SelectFigure(const uint8 &_cellPosition)
{
	auto cellState = GetCellInfo(gamefield, _cellPosition);

	if ((gameState == EChessGameState::Check) && (cellState.type != EChessFigureType::King)) {
		return false;
	}
	// Для короля особые правила передвижения
	PrepareFigureSteps(_cellPosition, (cellState.type == EChessFigureType::King));
	board->OnCellSelect(_cellPosition);
	board->ShowHints(GetFigureSteps());

	if (currentTeamPlay == EChessTeam::Team1) {
		onSelectCell.Broadcast(cellState.type, cellState.team, _cellPosition);
	}
	return true;
}

void UChessGameInstance::ClearFigure(const uint8 &_position)
{
	board->OnCellClear(_position);
	board->ClearHints();
	ClearFigureSteps();
}

bool UChessGameInstance::ValidateMove(const uint8 & _positionFrom, const uint8 & _positionTo)
{
	if (figureSteps.Num() == 0) {
		return false;
	}

	auto from = GetCellInfo(gamefield, _positionFrom);
	bool returnResult = false;

	if (from.type == EChessFigureType::None) {
		return false;
	}

	for (const auto i : figureSteps) {
		if (i.uid == _positionTo) {
			returnResult = true;
			break;
		}
	}

	return returnResult;
}

void UChessGameInstance::MakeMove(const uint8 &_from, const uint8 &_to)
{
	auto cellFromState = GetCellInfo(gamefield, _from);
	auto cellToState = GetCellInfo(gamefield, _to);
	// Нельзя ходить за другого игрока
	if (cellFromState.team != currentTeamPlay) {
		ClearFigure(_from);
		return;
	}

	Gamefield_Update(_from, 0);
	// Мы забрали фигуру противника
	if (cellToState.type != EChessFigureType::None) {
		board->RemoveFigure(_to);
	}
	Gamefield_Update(_to, cellFromState.ToUint8());

	ClearFigure(_from);
	board->OnFigureMove(_from, _to);

	onPlayerMakeStep.Broadcast(currentTeamPlay, _from, _to);

	PostMoveCheck();
	StepEnd();
}

TArray<uint8> UChessGameInstance::GetInitGamefield()
{
	using e = EChessFigureType;
	auto t0 = [](const EChessFigureType &_e) -> uint8 { return static_cast<uint8>(_e); };
	auto t1 = [](const EChessFigureType &_e) -> uint8 { return (static_cast<uint8>(_e) + static_cast<uint8>(EChessTeam::Team1)); };
	auto t2 = [](const EChessFigureType &_e) -> uint8 { return (static_cast<uint8>(_e) + static_cast<uint8>(EChessTeam::Team2)); };
	//
	static TArray<uint8> startGamefield{
		t1(e::Rook), t1(e::Knight), t1(e::Bishop), t1(e::Queen), t1(e::King),  t1(e::Bishop), t1(e::Knight), t1(e::Rook),
		t1(e::Pawn), t1(e::Pawn),   t1(e::Pawn),   t1(e::Pawn),  t1(e::Pawn),  t1(e::Pawn),   t1(e::Pawn),   t1(e::Pawn),
		t0(e::None), t0(e::None),   t0(e::None),   t0(e::None),  t0(e::None),  t0(e::None),   t0(e::None),   t0(e::None),
		t0(e::None), t0(e::None),   t0(e::None),   t0(e::None),  t0(e::None),  t0(e::None),   t0(e::None),   t0(e::None),
		t0(e::None), t0(e::None),   t0(e::None),   t0(e::None),  t0(e::None),  t0(e::None),   t0(e::None),   t0(e::None),
		t0(e::None), t0(e::None),   t0(e::None),   t0(e::None),  t0(e::None),  t0(e::None),   t0(e::None),   t0(e::None),
		t2(e::Pawn), t2(e::Pawn),   t2(e::Pawn),   t2(e::Pawn),  t2(e::Pawn),  t2(e::Pawn),   t2(e::Pawn),   t2(e::Pawn),
		t2(e::Rook), t2(e::Knight), t2(e::Bishop), t2(e::Queen), t2(e::King),  t2(e::Bishop), t2(e::Knight), t2(e::Rook),
	};
	return startGamefield;
}

void UChessGameInstance::PrepareFigureSteps(const uint8 &_cell, bool bIsKing)
{
	BoardLogic::GetPossibleMovesOnBoard(gamefield, _cell, figureSteps);
	if (bIsKing) {
		auto kingPos = FindKing(currentTeamPlay);

		BoardLogic::GetPossibleMovesOnBoardForKing(gamefield, kingPos, figureSteps);
	}
}


GamefieldCellInfo UChessGameInstance::FindKing(const EChessTeam & _team)
{
	GamefieldCellInfo king;

	for (int i = 0; i < 64; ++i)
	{
		auto cellState = GetCellInfo(gamefield, i);

		if ((cellState.type == EChessFigureType::King) && (cellState.team == _team)) {
			king = cellState;
			break;
		}
	}

	return king;
}

void UChessGameInstance::PostMoveCheck()
{
	// Смотрим а вдруг вражеская пешка добралась до конца поля...
	for (int i = 0; i < 64; ++i) {
		auto cellXY = ToXY(i);

		if (cellXY.y != 0 && cellXY.y != 7) {
			continue;
		}

		auto cellState = GetCellInfo(gamefield, i);

		if (cellState.type == EChessFigureType::Pawn) {
			// Можно сделать выбор с gui в кого превратить пешку ...
			board->TransformFigure(i, EChessFigureType::Queen, currentTeamPlay);
			cellState.type = EChessFigureType::Queen;
			Gamefield_Update(i, cellState.ToUint8());
		}
	}
	// Проверяем сделан ли 'шаг' вражескому королю
	auto enemyKingPosition = FindKing(currentTeamPlay == EChessTeam::Team1 ? EChessTeam::Team2 : EChessTeam::Team1);
	bool bKingCheck = false;

	for (int i = 0; i < 64; ++i) {
		auto cellState = GetCellInfo(gamefield, i);

		if ((cellState.team != currentTeamPlay) || cellState.type == EChessFigureType::None) {
			continue;
		}

		PrepareFigureSteps(i, false);

		for (int j = 0; j < figureSteps.Num(); ++j) {
			// Шаг обнаружен.
			if (figureSteps[j].uid == enemyKingPosition.position) {
				bKingCheck = true;
				break;
			}
		}

		ClearFigureSteps();

		if (bKingCheck) {
			break;
		}
	}
	// Объявляем Шаг.
	if (bKingCheck) {
		ChangeGameState(EChessGameState::Check);
	}
}

void UChessGameInstance::ChangeGameState(const EChessGameState & _newState, bool bIsNeedBroadcast)
{
	auto oldgameState = gameState;

	gameState = _newState;

	if (bIsNeedBroadcast) {
		onGameStateChange.Broadcast(currentTeamPlay, oldgameState, _newState);
	}
}

void UChessGameInstance::StepEnd()
{
	if (currentTeamPlay == EChessTeam::Team1) {
		currentTeamPlay = EChessTeam::Team2;
		AiStep();
	}
	else {
		currentTeamPlay = EChessTeam::Team1;
	}
}

void UChessGameInstance::AiStep()
{
	struct CellStepCheck {
		uint8 from;
		uint8 to;
		EChessFigureType priority;
	};

	auto &aiTeam = currentTeamPlay;
	// Нам шаг не поставили ...
	if (gameState == EChessGameState::Normal) {
		auto iter = 0;
		TArray<CellStepCheck> possibleMoves;

		// Ищем все возможные ходы
		while (iter < 64) {
			auto cellState = GetCellInfo(gamefield, iter);

			if ((cellState.team != aiTeam) || (cellState.type == EChessFigureType::None)) {
				++iter;
				continue;
			}
			PrepareFigureSteps(iter, cellState.type == EChessFigureType::King);

			for (auto &cellHint : figureSteps) {
				auto cellHintState = GetCellInfo(gamefield, cellHint.uid);

				if (cellHintState.team != aiTeam) {
					possibleMoves.Add({ static_cast<uint8>(iter), cellHint.uid, cellHintState.type });
				}
			}

			ClearFigureSteps();
			++iter;
		}
		// Делаем рандомный ход
		if (possibleMoves.Num() > 0) {
			bool bRandomIsWorking = true;
			while (bRandomIsWorking) {
				auto rand = FMath::RandRange(0, possibleMoves.Num() - 1);
				auto &randomStep = possibleMoves[rand];

				if (SelectFigure(randomStep.from)) {
					if (ValidateMove(randomStep.from, randomStep.to)) {
						ClearFigure(randomStep.from);
						MakeMove(randomStep.from, randomStep.to);
						bRandomIsWorking = false;
					}
					else {
						ClearFigure(randomStep.from);
					}
				}
			}
		}
	}
	else if (gameState == EChessGameState::Check) {
		auto king = FindKing(aiTeam);

		SelectFigure(king.position);
		// Если есть куда уйти
		if (figureSteps.Num() > 0) {
			ChangeGameState(EChessGameState::Normal, false);

			while (true) {
				auto rand = FMath::RandRange(0, figureSteps.Num() - 1);
				auto &randomStep = figureSteps[rand];
				auto cellObjTo = GetCellInfo(gamefield, randomStep.uid);

				if (ValidateMove(king.position, cellObjTo.position)) {
					MakeMove(king.position, randomStep.uid);
					break;
				}
			}
			ClearFigure(king.position);
		}
		else {
			ChangeGameState(EChessGameState::Checkmate);
		}
	}
}

