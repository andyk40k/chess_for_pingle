#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "../ChessEngine/Enums.h"
//
#include "IChessboard.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UIChessboard : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class CHESS_FOR_PINGLE_API IIChessboard
{
	GENERATED_BODY()

public:

	// 0
	virtual void InitBoard(const TArray<uint8> &_gamefield) = 0;

	// 0
	virtual void OnCellHover(const uint8 &_position) = 0;

	// 0
	virtual void OnCellSelect(const uint8 &_position) = 0;

	// 0
	virtual void OnCellClear(const uint8 &_position) = 0;

	// 0
	virtual void OnFigureMove(const uint8 &_cellFrom, const uint8 &_cellTo) = 0;

	// 0
	virtual void RemoveFigure(const uint8 &_position) = 0;

	// 0
	virtual void TransformFigure(const uint8 &_position, const EChessFigureType &_toType, const EChessTeam &_team) = 0;

	// 0 
	virtual void ShowHints(const TArray<FCellAction> &_data) = 0;

	// 0
	virtual void ClearHints() = 0;
};
