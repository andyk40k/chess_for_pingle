#pragma once

#include "CoreMinimal.h"
//
#include "Enums.h"

/**
 * 
 */
class CHESS_FOR_PINGLE_API BoardLogic
{
public:
	BoardLogic() = delete;
	~BoardLogic() = default;

public:
	// ...
	static void GetPossibleMoves(TArray<MoveRule> &_outputData, const uint8 &_boardPosition, const EChessFigureType &_figureType, const EChessTeam &_team);

	// ...
	static void GetPossibleMovesOnBoard(const TArray<uint8> &_gamefield, const uint8 &_cell, TArray<FCellAction> &_outputData);	
	
	// ...
	static void GetPossibleMovesOnBoardForKing(const TArray<uint8> &_gamefield, const GamefieldCellInfo &_kingState, TArray<FCellAction> &_outputData);

private:
	// ...
	static void Generate(const TArray<int32> &_rules, const EChessFigureType &_figureType, TArray<MoveRule> &_outputData, const int32 &_currentFigurePos, const EChessTeam &_team);

	// Правила перемещения для Пешки
	static TArray<int32> &RulePawn();

	// Правила перемещения для Коня
	static TArray<int32> &RuleKnight();

	// Правила перемещения для Слона
	static TArray<int32> &RuleBishop();

	// Правила перемещения для Ладьи
	static TArray<int32> &RuleRook();

	// Правила перемещения для Ферзя
	static TArray<int32> &RuleQueen();

	// Правила перемещения для Короля
	static TArray<int32> &RuleKing();
};
