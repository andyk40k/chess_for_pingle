#include "AChessboard.h"
//
#include "../ChessEngine/Enums.h"

// Sets default values
AChessboard::AChessboard()
	:
	xIndent(10),
	yIndent(10),
	obj_team1(
		{
			{EChessFigureType::Pawn, AFigureBase::StaticClass()},
			{EChessFigureType::Knight, AFigureBase::StaticClass()},
			{EChessFigureType::Bishop, AFigureBase::StaticClass()},
			{EChessFigureType::Rook, AFigureBase::StaticClass()},
			{EChessFigureType::Queen, AFigureBase::StaticClass()},
			{EChessFigureType::King, AFigureBase::StaticClass()},
		}
		),
	obj_team2(
		{
			{EChessFigureType::Pawn, AFigureBase::StaticClass()},
			{EChessFigureType::Knight, AFigureBase::StaticClass()},
			{EChessFigureType::Bishop, AFigureBase::StaticClass()},
			{EChessFigureType::Rook, AFigureBase::StaticClass()},
			{EChessFigureType::Queen, AFigureBase::StaticClass()},
			{EChessFigureType::King, AFigureBase::StaticClass()},
		}
		)//,
	//chessGameState(EChessGameState::None)
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	//
	root = CreateDefaultSubobject<USceneComponent>("root");
	RootComponent = root;
	//
	cellsRoot = CreateDefaultSubobject<USceneComponent>("cellsRoot");
	cellsRoot->SetupAttachment(RootComponent);
	figuresRoot = CreateDefaultSubobject<USceneComponent>("figuresRoot");
	figuresRoot->SetupAttachment(RootComponent);
	figuresRoot->SetRelativeLocation(cellsRoot->GetRelativeLocation() + FVector(0, 0, 20));
}

// Called when the game starts or when spawned
void AChessboard::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AChessboard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void AChessboard::InitBoard(const TArray<uint8>& _gamefield)
{
	if (cells.Num() == 0) {
		GenerateChessboard();
	}

	if (figures.Num() != 0) {
		for (int i = 0; i < 64; ++i) {
			RemoveFigure(i);
		}
	}

	SpawnFigures(_gamefield);
}
void AChessboard::OnCellHover(const uint8 & _position)
{
	auto cellObj = GetCellByUid(_position);

	cellObj->Select(materialHover);
}

void AChessboard::OnCellSelect(const uint8 & _position)
{
	auto cellObj = GetCellByUid(_position);

	cellObj->Select(materialSelected);
}

void AChessboard::OnCellClear(const uint8 & _position)
{
	auto cellObj = GetCellByUid(_position);

	cellObj->Clear();
}

void AChessboard::OnFigureMove(const uint8 &_cellFrom, const uint8 &_cellTo)
{
	auto figureFrom = GetFigureObj(_cellFrom);
	auto cellTo = GetCellByUid(_cellTo);

	FVector oMin, oMax;

	cellTo->GetActorBounds(false, oMin, oMax);
	figureFrom->SetActorLocation(cellTo->GetActorLocation() + FVector(0, 0, oMax.Z) + 4);
	figureFrom->boardPosition = _cellTo;
}

void AChessboard::RemoveFigure(const uint8 &_position)
{
	auto figureObj = GetFigureObj(_position);

	if (figureObj) {
		figures.Remove(figureObj);
		figureObj->Destroy();
	}
}

void AChessboard::TransformFigure(const uint8 &_position, const EChessFigureType &_toType, const EChessTeam &_team)
{
	auto neededFigureTeam = ((_team == EChessTeam::Team1) ? obj_team1 : obj_team2);
	auto figureToType = neededFigureTeam.Find(_toType);
	auto figure = GetFigureObj(_position);

	figure->figureMesh->SetStaticMesh(figureToType->GetDefaultObject()->figureMesh->GetStaticMesh());
	figure->figureType = _toType;
}

void AChessboard::ShowHints(const TArray<FCellAction>& _data)
{
	for (const auto &i : _data) {
		auto cell = GetCellByUid(i.uid);

		cell->Hover(i.action == EChessCellState::Select ? materialCanStep : materialAttack);
		cellsHints.Add(cell);
	}
}

void AChessboard::ClearHints()
{
	for (const auto &i : cellsHints) {
		i->Clear();
	}
	cellsHints.Reset();
}

#pragma optimize("", off)

void AChessboard::GenerateChessboard()
{
	UWorld* world = GetWorld();
	uint32 x = 0;
	uint32 y = 0;
	bool bIsEven = true;
	FVector spawnLocation(0, 0, 0);
	FActorSpawnParameters SpawnParams;
	//
	SpawnParams.Owner = this;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	auto c = FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false);
	//
	for (int32 i = 0; i < 64; ++i)
	{
		if (x % 8 == 0) {
			bIsEven = !bIsEven;
			++y;
			x = 1;
		}
		else {
			++x;
		}
		auto selectedObj = bIsEven ? cellEven : cellOdd;
		auto spawnedObj = world->SpawnActor<ACellBase>(selectedObj, spawnLocation, GetActorRotation(), SpawnParams);
		FVector oMin, oMax;
		//
		spawnedObj->uid = i;
		spawnedObj->GetActorBounds(false, oMin, oMax);
		spawnLocation.X = -((oMax.X * 2) * x) + (xIndent * x);
		spawnLocation.Y = ((oMax.Y * 2) * y) + (yIndent * y);
		spawnedObj->AttachToComponent(cellsRoot, c);
		spawnedObj->SetActorRelativeLocation(spawnLocation);
		cells.Add(spawnedObj);
		//
		bIsEven = !bIsEven;
	}
}

void AChessboard::SpawnFigures(const TArray<uint8> &_gamefield)
{
	UWorld* world = GetWorld();
	auto c = FAttachmentTransformRules(EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, EAttachmentRule::KeepRelative, false);
	FActorSpawnParameters SpawnParams;
	//
	SpawnParams.Owner = this;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	auto ToEnum = [](const uint8 &_e) -> EChessFigureType {
		if (_e > static_cast<uint8>(EChessTeam::Team2))
			return static_cast<EChessFigureType>(_e - static_cast<uint8>(EChessTeam::Team2));
		//
		return static_cast<EChessFigureType>(_e - static_cast<uint8>(EChessTeam::Team1));
	};
	uint32 i = 0;
	while (i < 64)
	{
		if (_gamefield[i] != 0) {
			FVector oMin, oMax;
			cells[i]->GetActorBounds(false, oMin, oMax);
			//
			auto figure = _gamefield[i] < static_cast<uint8>(EChessTeam::Team2) ? obj_team1.Find(ToEnum(_gamefield[i])) : obj_team2.Find(ToEnum(_gamefield[i]));
			auto spawnedObj = world->SpawnActor<AFigureBase>(figure->Get(), FVector(0, 0, 0), cells[i]->GetActorRotation(), SpawnParams);
			//
			spawnedObj->AttachToComponent(figuresRoot, c);
			spawnedObj->SetActorLocation(cells[i]->GetActorLocation() + FVector(0, 0, oMax.Z) + 4);
			//
			spawnedObj->boardPosition = i;
			//
			figures.Add(spawnedObj);
		}
		++i;
	}
}
