#include "ACellBase.h"

// Sets default values
ACellBase::ACellBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	//
	mesh = CreateDefaultSubobject<UStaticMeshComponent>("mesh");
	RootComponent = mesh;
}

// Called when the game starts or when spawned
void ACellBase::BeginPlay()
{
	Super::BeginPlay();
	//
	defaultMaterial = mesh->GetMaterial(0);
}

// Called every frame
void ACellBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACellBase::Hover(UMaterialInterface * _material)
{
	mesh->SetMaterial(0, _material);
}

void ACellBase::Select(UMaterialInterface * _material)
{
	mesh->SetMaterial(0, _material);
}

void ACellBase::Clear()
{
	mesh->SetMaterial(0, defaultMaterial);
}


