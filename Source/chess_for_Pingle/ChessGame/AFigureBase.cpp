#include "AFigureBase.h"

// Sets default values
AFigureBase::AFigureBase()
	:
	figureType(EChessFigureType::None),
	boardPosition(0)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;
	//
	figureMesh = CreateDefaultSubobject<UStaticMeshComponent>("figureMesh");
	RootComponent = figureMesh;

	figureMesh->SetSimulatePhysics(false);
	figureMesh->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
	figureMesh->SetCollisionObjectType(ECollisionChannel::ECC_WorldStatic);
}

// Called when the game starts or when spawned
void AFigureBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFigureBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

