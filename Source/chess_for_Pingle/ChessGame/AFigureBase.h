﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//
#include "../ChessEngine/Enums.h"
#include "Components/StaticMeshComponent.h"
//
#include "AFigureBase.generated.h"

UCLASS()
class CHESS_FOR_PINGLE_API AFigureBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFigureBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


public:
	// Моделька фигуры
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "-Chess|Figure")
	UStaticMeshComponent* figureMesh;

	// Тип фигуры
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "-Chess|Figure")
	EChessFigureType figureType;

	// Позиция фигуры на доске
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "-Chess|Figure")
	uint8 boardPosition;
};
