#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//
#include "Components/StaticMeshComponent.h"
//
#include "ACellBase.generated.h"

UCLASS()
class CHESS_FOR_PINGLE_API ACellBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACellBase();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

// --- ---
public:
	//
	void Hover(UMaterialInterface *_material);
	//
	void Select(UMaterialInterface *_material);
	//
	void Clear();

public:
	// 
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "-Chess|Base")
	UStaticMeshComponent* mesh;
	// 
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "-Chess|Base")
	uint8 uid;

private:
	// 
	UPROPERTY()
	UMaterialInterface *defaultMaterial;
};
