#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
//
#include "../ChessEngine/Enums.h"
#include "../ChessEngine/IChessboard.h"
#include "ACellBase.h"
#include "AFigureBase.h"
//
#include "AChessboard.generated.h"

UCLASS()
class CHESS_FOR_PINGLE_API AChessboard : public AActor, public IIChessboard
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AChessboard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

// --- --- ---

public:
	void InitBoard(const TArray<uint8> &_gamefield) override;

	void OnCellHover(const uint8 &_position) override;

	void OnCellSelect(const uint8 &_position) override;

	void OnCellClear(const uint8 &_position) override;

	void OnFigureMove(const uint8 &_cellFrom, const uint8 &_cellTo) override;

	void RemoveFigure(const uint8 &_position) override;

	void TransformFigure(const uint8 &_position, const EChessFigureType &_toType, const EChessTeam &_team) override;

	void ShowHints(const TArray<FCellAction> &_data) override;

	void ClearHints() override;

public:
	// Root
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "-Chess|Chessboard")
	USceneComponent* root;

	// К этому компоненту аттачатся все клетки
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "-Chess|Chessboard")
	USceneComponent* cellsRoot;

	// К этому компоненту аттачатся все фигуры
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "-Chess|Chessboard")
	USceneComponent* figuresRoot;

	// Актор для четной клетки
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "-Chess|Cells")
	TSubclassOf<ACellBase> cellEven;

	// Актор для не четной клетки
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "-Chess|Cells")
	TSubclassOf<ACellBase> cellOdd;

	// 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "-Chess|Cells")
	UMaterialInterface *materialHover;

	// 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "-Chess|Cells")
	UMaterialInterface *materialCanStep;

	// 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "-Chess|Cells")
	UMaterialInterface *materialSelected;

	// 
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "-Chess|Cells")
	UMaterialInterface *materialAttack;

	// Длинна отступа между ячейками по оси x
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "-Chess|Cells|Props")
	uint8 xIndent;

	// Длинна отступа между ячейками по оси y
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "-Chess|Cells|Props")
	uint8 yIndent;

	// Фигуры для игрока 1
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "-Chess|Figures")
	TMap<EChessFigureType, TSubclassOf<AFigureBase>> obj_team1;

	// Фигуры для игрока 2
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "-Chess|Figures")
	TMap<EChessFigureType, TSubclassOf<AFigureBase>> obj_team2;

private:
	// Генерация шахматной доски
	void GenerateChessboard();

	// Спавним фигуры
	void SpawnFigures(const TArray<uint8> &_gamefield);

	//
	FORCEINLINE ACellBase *GetCellByUid(const uint8 &_cellUID) {
		auto r = cells.FindByPredicate([&](ACellBase *_cell) {return (_cell->uid == _cellUID); });
		return r ? *r : nullptr;
	};

	//
	FORCEINLINE AFigureBase *GetFigureObj(const uint8 &_position) {
		auto r = figures.FindByPredicate([&](AFigureBase *_figure) {return (_figure->boardPosition == _position); });
		return r ? *r : nullptr;
	};

private:
	// Ptr ячейки
	UPROPERTY()
	TArray<ACellBase *> cells;
	// Ptr ячейки которые подсвечиваются сейчас
	UPROPERTY()
	TArray<ACellBase *> cellsHints;
	// Ptr на фигуры
	UPROPERTY()
	TArray<AFigureBase *> figures;
};
