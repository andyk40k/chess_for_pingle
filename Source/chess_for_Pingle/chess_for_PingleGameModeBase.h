#pragma once

#include "CoreMinimal.h"
//
#include "GameFramework/GameModeBase.h"
#include "ChessEngine/ChessGameInstance.h"
//
#include "chess_for_PingleGameModeBase.generated.h"

UCLASS()
class CHESS_FOR_PINGLE_API Achess_for_PingleGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	Achess_for_PingleGameModeBase();

	virtual void BeginPlay() override;

	virtual void StartPlay() override;

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "-Chess|Game")
	void CreateNewChessGame();

	UFUNCTION(BlueprintCallable, BlueprintAuthorityOnly, Category = "-Chess|Game")
	UChessGameInstance *GetChessGame() { return chessGame; };

private:
	UPROPERTY()
	UChessGameInstance *chessGame;
};
