#pragma once

#include "CoreMinimal.h"
//
#include "GameFramework/PlayerController.h"
#include "ChessGame/ACellBase.h"
#include "ChessGameStateBase.h"
//
#include "ChessPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CHESS_FOR_PINGLE_API AChessPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AChessPlayerController();

protected:
	virtual void BeginPlay() override;

	virtual void SetupInputComponent() override;


public:
	void MyMove();

	// 
	UFUNCTION(BlueprintCallable, Category = "-Chess|KeyActions")
	void OnLClick();

	// 
	UFUNCTION(BlueprintCallable, Category = "-Chess|KeyActions")
	void OnRClick();

	// 
	UFUNCTION(BlueprintCallable, Category = "-Chess|KeyActions")
	void OnEsc();

private:
	UPROPERTY()
	ACellBase *lastSelectedCell;
};
