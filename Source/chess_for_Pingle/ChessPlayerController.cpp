﻿#include "ChessPlayerController.h"
//
#include "Engine/Engine.h"
#include "Engine/World.h"
#include "chess_for_PingleGameModeBase.h"

AChessPlayerController::AChessPlayerController() {
	bEnableClickEvents = true;
	bEnableTouchEvents = true;
	bShowMouseCursor = true;
	AutoReceiveInput = EAutoReceiveInput::Player0;
}

void AChessPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void AChessPlayerController::SetupInputComponent()
{
	//Super::SetupInputComponent();
	//
	//InputComponent->BindAction("LClick", IE_Pressed, this, &AChessPlayerController::OnLClick);
	//InputComponent->BindAction("RClick", IE_Pressed, this, &AChessPlayerController::OnRClick);
	//InputComponent->BindAction("Esc", IE_Pressed, this, &AChessPlayerController::OnEsc);
}

void AChessPlayerController::MyMove()
{
}

void AChessPlayerController::OnLClick()
{
	FHitResult Hit;
	//auto gameboard = GetWorld()->GetGameState<AChessGameStateBase>()->GetChessboard();
	auto gameMode = Cast<Achess_for_PingleGameModeBase>(GetWorld()->GetAuthGameMode());
	auto chessGame = gameMode->GetChessGame();

	GetHitResultUnderCursor(ECC_Visibility, false, Hit);

	if (Hit.bBlockingHit) {
		if (Hit.Actor != NULL) {
			auto cell = Cast<ACellBase>(Hit.GetActor());

			if (cell) {
				// Нет ранее выбраной клетки
				if (lastSelectedCell == nullptr) {
					if (chessGame->SelectFigure(cell->uid)) {
						lastSelectedCell = cell;
					}
				}
				// Игрок выбрал клетку которая уже была активна
				else if (lastSelectedCell && (cell == lastSelectedCell)) {
					chessGame->ClearFigure(lastSelectedCell->uid);
					lastSelectedCell = nullptr;
				}
				// Есть уже выбранная клетка
				else if (lastSelectedCell) {
					// Модем ли мы ходить в эту клетку ?
					if (chessGame->ValidateMove(lastSelectedCell->uid, cell->uid)) {
						chessGame->MakeMove(lastSelectedCell->uid, cell->uid);
					}
					chessGame->ClearFigure(lastSelectedCell->uid);
					lastSelectedCell = nullptr;
				}
			}
		}
	}
 }

void AChessPlayerController::OnRClick()
{

}

void AChessPlayerController::OnEsc()
{

}
